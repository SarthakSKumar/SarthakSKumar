<!---
Please consider starring the repo if you find this useful in any manner
or use it. It helps me a lot.
-->
<img src='README_Banner.webp' alt="banner"></img>
<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif">
# Hi, I am <a href = "https://linkedin.com/in/sarthakskumar">Sarthak</a>. Nice to see you here 👋
<b>A 19-year-old, pursuing B.Tech in Computer Science from [PES University](https://www.pes.edu).</b><br>
I am a passionate self-learner and an open-source enthusiast. I love exploring and building projects👨‍💻 using various technologies.<br>
I conduct workshops, events, and collaborative programs in science, innovation, computers, entrepreneurship, etc.

<a href = "https://app.daily.dev/sarthakskumar"><img align = "right" src="https://api.daily.dev/devcards/4acca7dd7d934f94b0b4753f12c44494.png?r=nmz" width="250" alt="Sarthak S Kumar's Dev Card"></a>

- 🔭 Check out my <a href="https://sarthakskumar.github.io"><b>Portfolio Website</b></a>
- 🌱 I look for opportunities to use my skills to solve challenging real-world problems.
- 🪶 I try to offer my contribution which realises the power of community.
- 👯 I am open for collaboration with creators, developers and tech enthusiasts.
- 🚢 Keen on developing my skill set and putting it to use.<br>
<hr>

## 📩 Connect with me
Don't hesitate to ping me🤝. If you are interested to discuss any further, I'm always open for a conversation!!✔ Shoot me an email <a href = "mailto:sskworld9742@gmail.com"><b>here</b><br><br>
<a href = "https://linkedin.com/in/sarthakskumar"><img src = "https://skillicons.dev/icons?i=linkedin&theme=dark" height = 38></a>
<a href = "https://instagram.com/sarthakskumar"><img src = "https://skillicons.dev/icons?i=instagram&theme=dark" height = 38></a>
<a href = "https://discordapp.com/users/907567549410050078"><img src = "https://skillicons.dev/icons?i=discord&theme=dark" height = 38></a>
<a href = "https://twitter.com/SarthakSKumar2"><img src = "https://skillicons.dev/icons?i=twitter&theme=dark" height = 38></a>
<hr>

## ⚡ Languages, Tools and Technologies
<a href = "https://github.com/SarthakSKumar"><img src = "https://skillicons.dev/icons?i=c,java,cpp,py,html,css,js,bash,vscode,mongodb,codepen,django,firebase,netlify,raspberrypi,androidstudio,flask,githubactions,md,nextjs,redis,redux,regex,svg,jquery,express,tensorflow,nodejs,heroku,arduino,tailwind,vercel,git,bootstrap,ps,cloudflare,linux,pr,materialui,github,react,figma&theme=dark&perline=21" height = 90></a>
<hr>

## 🥇 Open Source Projects-Open for Contributions
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat&logo=github)](https://github.com/SarthakSKumar) [![Open Source Love](https://img.shields.io/badge/Open%20Source-%F0%9F%A4%8D-Green)](https://github.com/SarthakSKumar)
| Project:octocat: | Stars✨| Forks🍴 | Issues🐛 | Open PRs:bell: | Closed PRs:fire:
|----------------- |-------------------|---|---|---|---
| [**IoT Projects and Scripts**](https://github.com/SarthakSKumar/IoT-Projects-and-Scripts) | [![GitHub Stars](https://img.shields.io/github/stars/SarthakSKumar/IoT-Projects-and-Scripts?style=flat-square&labelColor=343b41)](https://github.com/SarthakSKumar/IoT-Projects-and-Scripts/stars) | [![GitHub Forks](https://img.shields.io/github/forks/SarthakSKumar/IoT-Projects-and-Scripts?style=flat-square&labelColor=343b41)](https://github.com/SarthakSKumar/IoT-Projects-and-Scripts/forks) | [![GitHub Issues](https://img.shields.io/github/issues/SarthakSKumar/IoT-Projects-and-Scripts?style=flat-square)](https://github.com/SarthakSKumar/IoT-Projects-and-Scripts/issues) | [![GitHub Open Pull Requests](https://img.shields.io/github/issues-pr/SarthakSKumar/IoT-Projects-and-Scripts?style=flat&logo=github)](https://github.com/SarthakSKumar/IoT-Projects-and-Scripts/pulls) | [![GitHub Closed Pull Requests](https://img.shields.io/github/issues-pr-closed/SarthakSKumar/IoT-Projects-and-Scripts?style=flat&color=critical&logo=github)](https://github.com/SarthakSKumar/IoT-Projects-and-Scripts/pulls?q=is%3Apr+is%3Aclosed)
| [**Chatroom-App**](https://github.com/SarthakSKumar/Chatroom-App) | [![GitHub Stars](https://img.shields.io/github/stars/SarthakSKumar/Chatroom-App?style=flat-square&labelColor=343b41)](https://github.com/SarthakSKumar/Chatroom-App/stars) | [![GitHub Forks](https://img.shields.io/github/forks/SarthakSKumar/Chatroom-App?style=flat-square&labelColor=343b41)](https://github.com/SarthakSKumar/Chatroom-App/forks) | [![GitHub Issues](https://img.shields.io/github/issues/SarthakSKumar/Chatroom-App?style=flat-square)](https://github.com/SarthakSKumar/Chatroom-App/issues) | [![GitHub Open Pull Requests](https://img.shields.io/github/issues-pr/SarthakSKumar/Chatroom-App?style=flat&logo=github)](https://github.com/SarthakSKumar/Chatroom-App/pulls) | [![GitHub Closed Pull Requests](https://img.shields.io/github/issues-pr-closed/SarthakSKumar/Chatroom-App?style=flat&color=critical&logo=github)](https://github.com/SarthakSKumar/Chatroom-App/pulls?q=is%3Apr+is%3Aclosed)
<hr>

## 📊 Metrics
<table>
	<tr>
		<td colspan = "2"><a href = "https://sarthakskumar.bio.link"><img src="https://github-readme-activity-graph.cyclic.app/graph?username=SarthakSKumar&bg_color=2e3440&hide_border=true&point=false&line=88c0d0&radius=8&area=true&area_color=88c0d0&title_color=ffffff&color=ffffff"></a></td>
	</tr>
	<tr>
		<td><a href="https://linkedin.com/in/sarthakskumar"><img src="https://github-readme-stats.vercel.app/api?username=SarthakSKumar&hide_border=true&include_all_commits=true&count_private=true&show_icons=true&line_height=20&theme=nord"></a></td>
		<td><a href="https://wakatime.com/@sarthakskumar"><img src="https://github-readme-stats.vercel.app/api/wakatime?username=sarthakskumar&langs_count=6&hide_border=true&border_radius=4.5&layout=compact&theme=nord"></a></td>
	</tr>
	<tr>
		<td colspan = "2"><a href="https://instagram.com/sarthakskumar"><img width=100% src="https://github-profile-trophy.vercel.app/?username=SarthakSKumar&hide_border=true&count_private=true&column=8&theme=nord&no-frame=true"></a></td>
	</tr>
	<tr>
		<td><a href="https://wakatime.com/@sarthakskumar"><img src="https://wakatime.com/share/@sarthakskumar/7d17f360-8efd-4581-8466-2a44cd850351.svg"></a>			</td>
		<td><a href="https://wakatime.com/@sarthakskumar"><img src="https://wakatime.com/share/@sarthakskumar/2b3045cc-3591-4c2d-bc9e-9218d8fd8117.svg"></a>			</td>
	</tr>
	</table>
<hr>
	
## ✒️ Recent GitHub Activity
<!--START_SECTION:activity-->
1. 💪 Opened PR [#5](https://github.com/satyamksharma/Sublit-Programs/pull/5) in [satyamksharma/Sublit-Programs](https://github.com/satyamksharma/Sublit-Programs)
2. 🎉 Merged PR [#1](https://github.com/SarthakSKumar/Sublit-Programs/pull/1) in [SarthakSKumar/Sublit-Programs](https://github.com/SarthakSKumar/Sublit-Programs)
3. 💪 Opened PR [#1](https://github.com/SarthakSKumar/Sublit-Programs/pull/1) in [SarthakSKumar/Sublit-Programs](https://github.com/SarthakSKumar/Sublit-Programs)
4. ❗️ Opened issue [#4](https://github.com/satyamksharma/Sublit-Programs/issues/4) in [satyamksharma/Sublit-Programs](https://github.com/satyamksharma/Sublit-Programs)
5. 🗣 Commented on [#2](https://github.com/satyamksharma/Sublit-Programs/issues/2) in [satyamksharma/Sublit-Programs](https://github.com/satyamksharma/Sublit-Programs)
<!--END_SECTION:activity-->
  </b>
<hr>

## 📝 Testimonials
<table>
	<tr align="center">
		<td><b>Name</b></td>
		<td><b>Thoughts on me</b></td>
		<td><b>Designation/Activity</b></td>
	</tr>
	<tr>
		<td><a href="https://www.linkedin.com/in/ritika-chauhan-687055208"><b>Ritika Chauhan</b></a></td>
		<td>Sarthak joined CS Mock as a campus manager, and he outshone from day one. His dedication towards work and work ethics are commendable. 
He was very proactive and great team player throughout his internship. 
He always gave suggestions in ways the work cultute can be improved for the comapny and to become more accommodating for all.
I will be more than happy to recommend Sarthak if you are looking for someone with enduring dedication towards his work.</td>
		<td>HR Executive</td>
	</tr>
	<tr>
		<td><a href="https://www.linkedin.com/in/rajath-01b605213"><b>Rajath Kumar J</b></a></td>
		<td>If anyone one is looking for creative minded programmer then you must take a look at sarthaks profile. My work experience with Sarthak was filled with right guidance and satisfaction. A mentor , coder and a person with golden heart</td>
		<td>Digital Marketing, Copywriting</td>
	</tr>
	<tr>
		<td><a href="https://www.linkedin.com/in/sherone-d-souza-9a497b180"><b>Sherone D'Souza</b></a></td>
		<td>Talented yet creative, Sarthak is an out of the box thinker and a great leader. He is indeed a pleasure to work with.
Besides having a great sense of humor he is a systematic organizer and an innovative programmer.
If your looking for anyone to spice up your experience, Sarthak is who I would recommend.</td>
		<td>CSE Student, MIT Manipal</td>
	</tr>
	<table>
<hr>
<a href = "https://www.holopin.io/@sarthakskumar"><img src = "https://holopin.me/sarthakskumar"></a>
<hr>
<div align = "center">
<h3><b>Visitors Count 👁️</b></h3>
<img width = 25% src = "https://profile-counter.glitch.me/{SarthakSKumar}/count.svg">
 
### Show some ❤️ by starring 🌟 some of my repos!
</div>
<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif">
<img src="https://cr-ss-service.azurewebsites.net/api/ScreenShot?widget=activity&username=SarthakSKumar">
